import os

base_config = {
    'development': {
        'srcDB': {
            'db': 'XMSTesting',
            'user': 'root',
            'password': 'root',
            'use_unicode': True,
            'charset': 'utf8'
        },
        'dstDB': {
            'db': 'xmsreport',
            'user': 'root',
            'password': 'root',
            'use_unicode': True,
            'charset': 'utf8'
        },
        'webhook_code': 'c8tchh4bs4f5jduu',
        'hostname': 'xmtextiles.bitrix24.eu',
        'webhook_user': 189,
        'base_path': '/Users/bootoffav/dev/python/xmsreport-project'
    },
    'production': {
        'srcDB': {
            'db': 'XMSTesting',
            'user': 'root',
            'password': 'HOv5g5aquZnMQMIIvzy3',
            'use_unicode': True,
            'charset': 'utf8'
        },
        'dstDB': {
            'db': 'xmsreport',
            'user': 'root',
            'password': 'HOv5g5aquZnMQMIIvzy3',
            'use_unicode': True,
            'charset': 'utf8'
        },
        'webhook_code': 'c8tchh4bs4f5jduu',
        'hostname': 'xmtextiles.bitrix24.eu',
        'webhook_user': 189,
        'base_path': '/home/bootoffav/xmsreport_collector'
    }
}

config = base_config[os.environ.get('xmsreport', 'production')]
