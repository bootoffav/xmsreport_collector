import warnings
from openpyxl import load_workbook
from collections import namedtuple


class Test:
    def __init__(self, row, file='files/current.xlsx'):
        self.file_id = row.file_id
        self.task_id = row.task_id
        self.folder_id = row.folder_id
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            self.sheet = load_workbook(file).active

    @property
    def name(self):
        return self.sheet.cell(row=3, column=1).value

    @property
    def checking_date(self) -> str:
        return str(self.sheet.cell(row=3, column=2).value)

    @property
    def items(self):

        def calculate_density(width, weight) -> int:
            if width is None or weight is None:
                return None

            return round(weight*(1000/width)*10)

        def calculate_empty_box_weight(full_box_weight, weight) -> int:
            if full_box_weight is None or weight is None:
                return None

            return round(full_box_weight - weight * 10, 2)

        def itemNameIsCorrect(item_name):
            return str(item_name).isascii()

        def replaceIncorrectChars(item_name):
            return item_name.replace('/', '-');

        def is_unique() -> bool:
            return item_name not in (str(item[0]).strip() for item in items)

        items = []
        for row in self.sheet.iter_rows(min_row=3, min_col=3):
            if row[1].value is None:
                row[1].value = '0'
            item = tuple(x.value for x in row)
            if not itemNameIsCorrect(item[0]):
                continue
            item_name = str(int(item[0])).strip() if type(item[0]) == float else str(
                item[0]).strip()  # sometimes in Excel name is saved as float cell format
            item_name = replaceIncorrectChars(item_name)

            if is_unique():
                try:
                    item = Item(*item)._replace(
                        name=item_name,
                        density=calculate_density(
                            width=item[5], weight=item[7]),
                        empty_box_weight=calculate_empty_box_weight(full_box_weight=item[11],
                                                                    weight=item[7])
                    )
                except TypeError as i:
                    print(i, self.name, self.task_id,
                          'problem with items in excel')
                items.append(item)

        return items


Item = namedtuple('Item', [
    'name', 'quantity', 'type', 'factory',
    'lot_number', 'width', 'length', 'weight', 'density', 'empty_box_weight',
    'box_size', 'full_box_weight', 'R_before_washing_in_office',
    'R_after_5_washing_60_degrees_in_office', 'R_before_washing_at_factory',
    'R_after_25_washings_60_degrees_at_factory',
    'R_after_25_washings_90_degrees_at_factory',
    'R_after_50_washings_60_degrees_at_factory', 'Afterflame_before_washings'
])
