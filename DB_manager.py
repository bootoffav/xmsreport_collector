from warnings import catch_warnings, simplefilter
from collections import namedtuple
import pymysql
from config import config
import sys
import datetime

dst_db = pymysql.connect(**config['dstDB'])
src_db = pymysql.connect(**config['srcDB'])


def find_excel_files() -> tuple:
    XMSTesting_row = namedtuple('XMSTesting_row',
                                ['id', 'task_id', 'folder_id', 'file_id'])

    try:
        sql = f"SELECT id, taskId, folderId, fileId from map WHERE excelFile = '1' AND fileId = '{sys.argv[1]}'"
    except IndexError:
        sql = "SELECT id, taskId, folderId, fileId from map WHERE excelFile = '1'"
    with src_db.cursor() as cur:
        cur.execute(sql)
        return (XMSTesting_row(*item) for item in cur)


def record_exists(query=None) -> bool:
    with dst_db.cursor() as cur:
        cur.execute(query)

    if cur.rowcount:
        for row in cur:
            id = row[0]
    else:
        id = None

    return id


def save_test(test) -> None:
    id = record_exists(
        f"SELECT id from `tests` WHERE fileId = '{test.file_id}'")

    if (id):
        query = f"UPDATE `tests` SET `name` = '{test.name}', \
                 `checking_date` = '{test.checking_date}', \
                 `taskId` = '{test.task_id}', \
                 `folderId` = '{test.folder_id}', \
                 `updated_at` = '{datetime.datetime.now()}' \
                 WHERE `fileId` = '{test.file_id}'"
    else:
        query = f"INSERT INTO `tests` (`name`, `checking_date`, `fileId`, \
                  `taskId`, `folderId`, `updated_at`) VALUES ('{test.name}', \
                  '{test.checking_date}', '{test.file_id}', \
                  '{test.task_id}', '{test.folder_id}', '{datetime.datetime.now()}')"

    with dst_db.cursor() as cur:
        with catch_warnings():
            simplefilter('ignore', pymysql.Warning)
            cur.execute(query)
    dst_db.commit()

    test.id = id or cur.lastrowid


def save_items(test) -> None:
    for item in test.items:
        try:
            idOfExistedRecord = record_exists(f"SELECT id from `items` \
                                WHERE `test_id` = '{test.id}' AND `name` = '{item.name}'")
        except AttributeError:
            continue

        if idOfExistedRecord:
            with dst_db.cursor() as cur:
                cur.execute(f"SELECT manually_edited from `items` \
                                WHERE `id` = '{idOfExistedRecord}'")

            if cur.rowcount:
                for row in cur:
                    manually_edited = row[0]

            if manually_edited:
                continue

            query = f"UPDATE `items` SET `name` = %s, \
                    `quantity` = %s, \
                    `type` = %s, `factory` = %s, `lot_number` = %s, \
                    `width` = %s, `length` = %s, `weight` = %s, \
                    `density` = %s, `empty_box_weight` = %s, `box_size` = %s, \
                    `full_box_weight` = %s, `R_before_washing_in_office` = %s, \
                    `R_after_5_washing_60_degrees_in_office` = %s, \
                    `R_before_washing_at_factory` = %s, \
                    `R_after_25_washings_60_degrees_at_factory` = %s, \
                    `R_after_25_washings_90_degrees_at_factory` = %s, \
                    `R_after_50_washings_60_degrees_at_factory` = %s, \
                    `Afterflame_before_washings` = %s, `test_id` = %s, \
                    `updated_at` = '{datetime.datetime.now()}' \
                    WHERE `id` = %s"
        else:
            query = f"INSERT INTO `items` (`name`, `quantity`, `type`, `factory`, `lot_number`, \
                    `width`, `length`, `weight`, `density`, `empty_box_weight`, \
                    `box_size`, `full_box_weight`, `R_before_washing_in_office`, \
                    `R_after_5_washing_60_degrees_in_office`, \
                    `R_before_washing_at_factory`, \
                    `R_after_25_washings_60_degrees_at_factory`, \
                    `R_after_25_washings_90_degrees_at_factory`, \
                    `R_after_50_washings_60_degrees_at_factory`, \
                    `Afterflame_before_washings`, `test_id`, `manually_edited`, `updated_at`) \
                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, 0, '{datetime.datetime.now()}')"

        params = [*item]
        params.append(test.id)
        if idOfExistedRecord:
            params.append(idOfExistedRecord)

        with dst_db.cursor() as cur:
            cur.execute(query, params)
    dst_db.commit()


def delete_db_record(id, folder_id):
    #  from XMSTesting
    sql = f"DELETE FROM map WHERE id = '{id}'"
    with src_db.cursor() as cur:
        cur.execute(sql)
    src_db.commit()

    #  from xmsreport
    with dst_db.cursor() as cur:
        cur.execute(f"SELECT `id` FROM tests WHERE folderId = {folder_id}")
        test_id = cur.fetchone()[0]
        cur.execute(f'DELETE FROM items WHERE items.test_id IN ({test_id})')
        cur.execute(f'DELETE FROM tests WHERE tests.id IN ({test_id})')
    dst_db.commit()
