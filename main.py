import os
import time
import pymysql
from config import config
from Test import Test, Item
from B24_manager import B24_manager
from DB_manager import find_excel_files, save_test, save_items

if __name__ == '__main__':
    os.chdir(config['base_path'])
    b24 = B24_manager(config['webhook_code'], config['hostname'], config['webhook_user'])

    for row in find_excel_files():
        try:
            if b24.sync(row):
                b24.download_file(row.file_id)
                test = Test(row)
                save_test(test)
                save_items(test)
                os.remove('files/current.xlsx')
                time.sleep(3)
        except TypeError as e:
            print(e, row, 'type error')
        except (pymysql.err.IntegrityError, pymysql.err.InternalError) as e:
            print(e, row, 'intergiry error')
        except UnboundLocalError as e:
            print(e, row, 'unboundError')
        except Exception as e:
            print(e, '--common error--')
