from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

from .helpers import items_by_total_quantity
from .models import Test, Item


@login_required
def tests(request):
    tests = Test.objects.all().order_by('-checking_date')
    for test in tests:
        test.items = test.item_set.all()
    return render(request, 'xmstests/tests.html', {'tests': tests})


@login_required
def items(request):
    quantity_per_item = sorted(items_by_total_quantity(),
                               key=lambda x: x.name)

    items = ((item_name, quantity, Test.objects.filter(item__name=item_name))
             for item_name, quantity in quantity_per_item)

    return render(request, 'xmstests/items.html',
                  {'items': items})


@login_required
def test_delete(request, name):
    Test.objects.filter(name=name).delete()
    return redirect('/')


@login_required
def test_detail(request, name):

    def coerseToFloat(value):
        try:
            return float(value)
        except:
            return None

    if request.method == 'POST':
        item = Item.objects.get(id=int(request.POST.get('id')))
        item.quantity = request.POST.get('quantity')
        item.type = request.POST.get('type')
        item.factory = request.POST.get('factory')
        item.lot_number = request.POST.get('lot_number')
        item.width = request.POST.get('width')
        item.length = request.POST.get('length')
        item.weight = request.POST.get('weight')
        item.density = request.POST.get('density')
        item.empty_box_weight = request.POST.get('empty_box_weight')
        item.box_size = request.POST.get('box_size')
        item.full_box_weight = request.POST.get('full_box_weight')

        item.R_before_washing_in_office = coerseToFloat(
            request.POST.get('R_before_washing_in_office'))

        item.R_after_5_washing_60_degrees_in_office = coerseToFloat(
            request.POST.get('R_after_5_washing_60_degrees_in_office'))

        item.R_before_washing_at_factory = coerseToFloat(
            request.POST.get('R_before_washing_at_factory'))

        item.R_after_25_washings_60_degrees_at_factory = coerseToFloat(
            request.POST.get('R_after_25_washings_60_degrees_at_factory'))

        item.R_after_25_washings_90_degrees_at_factory = coerseToFloat(
            request.POST.get('R_after_25_washings_90_degrees_at_factory'))

        item.R_after_50_washings_60_degrees_at_factory = coerseToFloat(
            request.POST.get('R_after_50_washings_60_degrees_at_factory'))

        item.Afterflame_before_washings = coerseToFloat(
            request.POST.get('Afterflame_before_washings'))

        item.rolls_per_box = coerseToFloat(
            request.POST.get('rolls_per_box'))

        item.manually_edited = True
        item.save()

    test = Test.objects.get(name=name)
    return render(request, 'xmstests/test_detail.html',
                  {'test': test, 'items': test.item_set.all()})


@login_required
def item_delete(request, id):
    item = Item.objects.get(id=id)
    test = Test.objects.get(id=item.test_id)
    Item.objects.filter(id=id).delete()

    return redirect(f"/test/{test.name}/")


@login_required
def item_edit(request, name):
    item = Item.objects.get(id=request.GET.get('item'))
    return render(request, 'xmstests/item_edit.html',
                  {
                        'item': item,
                        'name': name,
                  })
