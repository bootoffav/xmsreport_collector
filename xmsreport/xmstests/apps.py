from django.apps import AppConfig


class XmstestsConfig(AppConfig):
    name = 'xmstests'
