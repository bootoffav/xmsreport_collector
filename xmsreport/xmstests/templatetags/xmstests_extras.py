from django import template

register = template.Library()


@register.filter
def draw(items, entity):
    items = list(items)
    output = ''

    while items:
        current_items = items[0:10]
        del items[0:10]
        for item in current_items:
            output += f'<small><a class="item-link" \
                        href="/{entity}/{item.name}">{item.name}</a></small>; '

    return output


@register.filter
def link_to_test(name):
    return name.split()[0]


@register.filter
def percent(value):
    return str(value) + '%' if not(value == '') else None


@register.filter
def digit(value):
    return str(value).replace(' ', '')
