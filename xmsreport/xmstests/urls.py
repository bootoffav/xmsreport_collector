from django.urls import path, re_path
from django.contrib.auth.decorators import login_required

from . import views
from .class_views.Dashboard import Dashboard
from .class_views.ItemDetail import ItemDetail

urlpatterns = [
    path('', login_required(Dashboard.as_view()), name='dashboard'),
    path('tests/', views.tests, name='tests'),
    path('items/', views.items, name='items'),
    path('test/<str:name>/', views.test_detail, name='test_detail'),
    path('test/<str:name>/edit/', views.item_edit, name='item_edit'),
    path('item/<str:id>/delete', views.item_delete, name='item_delete'),
    path('test/<str:name>/delete', views.test_delete, name='test_delete'),

    path('item/<str:item_name>/',
         login_required(ItemDetail.as_view()), name='item_detail'),

    re_path(r'^item/(?P<item_name>\d+\/\d+)/',
            login_required(ItemDetail.as_view()), name='item_detail')
]
