<script>
    let {{ chart }} = new Chart(document.getElementById('{{ chart }}'), {
        type: '{{ type }}',
        data: {
            labels: {{ labels|safe }},
            datasets: [{
                data: {{ values|safe }},
                backgroundColor: {{ colors|safe }}
            }]
        },
        options: {{ chart }}Options
    });
</script>