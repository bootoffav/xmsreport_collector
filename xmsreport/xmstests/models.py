from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE_CASCADE

from django.db import models


class Test(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE
    name = models.CharField(max_length=200)
    checking_date = models.DateField(max_length=200)
    taskId = models.CharField(max_length=11)
    fileId = models.CharField(max_length=11)
    folderId = models.CharField(max_length=11, null=True)
    updated_at = models.DateTimeField(False, False, null=True, editable=True)

    def __str__(self):
        return self.name

    @property
    def total_quantity(self):
        quantity = 0
        for item in self.item_set.all():
            quantity += int(item.quantity)

        return quantity

    class Meta():
        db_table = 'tests'


class Item(SafeDeleteModel):
    _safedelete_policy = SOFT_DELETE_CASCADE
    test = models.ForeignKey(Test, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    quantity = models.CharField(max_length=200, default='0')
    type = models.CharField(max_length=200, null=True)
    factory = models.CharField(max_length=200, null=True)
    lot_number = models.CharField(max_length=200, null=True)
    width = models.CharField(max_length=200, null=True)
    length = models.CharField(max_length=200, null=True)
    weight = models.CharField(max_length=200, null=True)
    density = models.CharField(max_length=200, null=True)
    empty_box_weight = models.CharField(max_length=200, null=True)
    box_size = models.CharField(max_length=200, null=True)
    full_box_weight = models.CharField(max_length=200, null=True)
    R_before_washing_in_office = models.CharField(max_length=200, null=True)
    R_after_5_washing_60_degrees_in_office = models.CharField(
        max_length=200, null=True)
    R_before_washing_at_factory = models.CharField(max_length=200, null=True)
    R_after_25_washings_60_degrees_at_factory = models.CharField(
        max_length=200, null=True)
    R_after_25_washings_90_degrees_at_factory = models.CharField(
        max_length=200, null=True)
    R_after_50_washings_60_degrees_at_factory = models.CharField(
        max_length=200, null=True)
    Afterflame_before_washings = models.CharField(max_length=200, null=True)
    manually_edited = models.BooleanField(null=False)
    rolls_per_box = models.IntegerField(null=True)
    updated_at = models.DateTimeField(False, False, null=True, editable=True)

    def __str__(self):
        return self.name

    class Meta():
        db_table = 'items'
