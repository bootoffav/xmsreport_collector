from collections import OrderedDict
from datetime import date
from operator import attrgetter

from dateutil import parser
from dateutil.relativedelta import relativedelta
from django.views import View
from django.shortcuts import render

from xmstests.models import Test, Item
from xmstests.helpers import gen_colors, items_by_total_quantity


class Dashboard(View):
    def get(self, request):
        return self.main(request, request.GET.get('start'), request.GET.get('end'))

    def post(self, request):
        return self.main(request, request.POST.get('start'), request.POST.get('end'))

    def main(self, request, startDate, endDate):
        # chart 1
        chart = OrderedDict()
        tests_of_months = []
        ranges = []
        if startDate and endDate:
            i = 0
            while parser.parse(endDate) - relativedelta(months=i) >= parser.parse(startDate):
                next = parser.parse(endDate) - relativedelta(months=i)
                ranges.append((date(next.year, next.month, 1),
                               date(next.year, next.month, 1) + relativedelta(day=31)))
                i += 1
        else:
            for i in range(12):
                next = date.today() - relativedelta(months=i)
                ranges.append((date(next.year, next.month, 1),
                               date(next.year, next.month, 1) + relativedelta(day=31)))

        for r in ranges:
            tests = Test.objects.filter(checking_date__range=r)
            month_name = r[0].strftime('%B %Y')
            chart[month_name] = 0
            tests_of_month = []
            for test in tests:
                chart[month_name] += test.total_quantity
                tests_of_month.append(
                    f'{test.name} ({test.total_quantity:,}m)'.replace(',', ' '))
            tests_of_months.append((month_name, tests_of_month))

        # chart 2
        quantity_per_item = items_by_total_quantity()

        items_table = sorted(quantity_per_item,
                             key=lambda x: x.name,
                             )
        return render(request, 'xmstests/dashboard.html', {
            'startDate': startDate or (date.today() - relativedelta(months=12)).strftime('%B %Y'),
            'endDate': endDate or date.today().strftime('%B %Y'),
            'ch1_labels': list(chart.keys()),
            'ch1_values': list(chart.values()),
            'ch1_colors': gen_colors(len(ranges)+1),
            'tests_of_months': tests_of_months,
            'halfs': (f":{len(ranges)//2}", f'{len(ranges)//2}:'),
            'halfs_items': (f":{len(items_table)//2}", f'{len(items_table)//2}:'),
            'ch2_labels': [attrgetter('name')(item)
                           for item in quantity_per_item],
            'ch2_values': [attrgetter('quantity')(item)
                           for item in quantity_per_item],
            'ch2_colors': gen_colors(len(quantity_per_item)),
            'items_table': items_table
        })
