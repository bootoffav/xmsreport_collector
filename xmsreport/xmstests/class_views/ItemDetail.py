from collections import OrderedDict
from datetime import date
import operator

from dateutil.relativedelta import relativedelta
from django.views import View
from django.shortcuts import render

from ..models import Test, Item
import xmstests.helpers as helpers


class ItemDetail(View):

    def get(self, request, item_name):
        tests_set = Test.objects.filter(item__name=item_name)
        tests = [(test, test.item_set.get(name=item_name))
                 for test in tests_set]
        tests = self.calculate_deteriorations(tests)

        item = {
            'name': item_name,
            'deviation': self.calculate_deviations(tests)
        }

        return render(request, 'xmstests/item_detail.html',
                      {'tests': tests, 'item': item})

    def calculate_deviations(self, items):
        deviation = {}
        deviations = ('weight', 'full_box_weight', 'empty_box_weight',
                      'R_before_washing_in_office',
                      'R_after_5_washing_60_degrees_in_office',
                      'R_before_washing_at_factory',
                      'R_after_25_washings_60_degrees_at_factory',
                      'R_after_25_washings_90_degrees_at_factory',
                      'R_after_50_washings_60_degrees_at_factory',
                      'deterioration_after_wash_in_office',
                      'deterioration_after_wash_at_factory')

        items = tuple(item[1] for item in items)

        for index in deviations:
            current_set = [operator.attrgetter(index)(items[i])
                           for i in range(len(items))]
            try:
                current_set = [float(i) for i in current_set]
            except (TypeError, ValueError):
                continue
            deviation[index] = round((max(current_set) - min(current_set)) /
                                     min(current_set) * 100, 1)

        return deviation

    def calculate_deteriorations(self, tests):

        def calculate(value_1, value_2):
            try:
                res = round((float(value_1)-float(value_2)) /
                            float(value_1) * 100, 1)
                return res
            except TypeError:
                return ''

        for _, item in tests:
            item.deterioration_after_wash_in_office = \
                calculate(item.R_after_5_washing_60_degrees_in_office,
                          item.R_after_25_washings_60_degrees_at_factory)

            item.deterioration_after_wash_at_factory = \
                calculate(item.R_before_washing_at_factory,
                          item.R_after_25_washings_90_degrees_at_factory)

        return tests
