from random import randrange
from collections import namedtuple

from .models import Item


Item_with_quantity = namedtuple('Item_with_quantity', ['name', 'quantity'])


def gen_colors(amount):
    return ['rgba(' + str(randrange(1, 255)) +
            ',' + str(randrange(1, 255)) +
            ',' + str(randrange(1, 255)) + ', .9)'
            for _ in range(amount)]


def items_by_total_quantity():
    quantity_per_item = []
    for distinct_name in Item.objects.values('name').distinct():
        quantity_per_item.append(
            Item_with_quantity(distinct_name['name'], 
            sum(int(item.quantity) for item in
            Item.objects.filter(name=distinct_name['name']))
        ))

    return quantity_per_item

