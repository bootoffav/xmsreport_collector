Chart.defaults.global.legend.display = false;

const baseChartOptions = {
    tooltips: {
        callbacks: {
            label(tooltipItems, data) {
                return data.labels[tooltipItems.index] + ": " + data.datasets[0].data[tooltipItems.index].toLocaleString().replace(',', ' ');
            }
        }
    }
}

const itemsChartOptions = baseChartOptions;
const quantityChartOptions = Object.assign({}, baseChartOptions, {
    xAxes: [{
        ticks: {
            beginAtZero: true,
            callback(value, index, values) {
                return value.toLocaleString().replace(',', ' ');
            }
        }
    }]
});

window.onload = () => {
    let deleteTest = document.getElementById('delete-test');
    if (deleteTest) {
        deleteTest.addEventListener('click', (event) => {
            if (!confirm('Do you really want to delete this test?')) {
                event.preventDefault();
            }
        });
    }
}
