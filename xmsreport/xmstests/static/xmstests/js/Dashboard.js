window.onload = () => {
  const elem = document.getElementById('dateRange');
  const dateRangePicker = new DateRangePicker(elem, {
    buttonClass: 'btn',
    autohide: true,
    allowOneSidedRange: true,
    format: 'MM yyyy',
    clearBtn: true,
    startView: 1,
  });
}