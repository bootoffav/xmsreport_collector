import shutil
from urllib.request import urlopen

from pybitrix24 import Bitrix24

from config import config
from DB_manager import delete_db_record


class B24_manager():

    def __init__(self, code, hostname, user_id):
        self.webhook_code = code
        self.instance = Bitrix24(hostname, user_id=user_id)

    def download_file(self, file_id):
        try:
            response = self.instance.call_webhook(self.webhook_code, 'disk.file.get',
                                                 params={'id': file_id})
            url = response['result']['DOWNLOAD_URL']
        except KeyError as e:
            exit(file_id, e, 'token expired')

        with open('files/current.xlsx', 'wb') as current_file:
            shutil.copyfileobj(urlopen(url), current_file)

    def sync(self, row) -> bool:
        folder_state = self.folder_state(row.folder_id)
        if folder_state == 'NOT_FOUND':
            self.delete_associated_task(row.task_id)
            delete_db_record(row.id, row.folder_id)
            return False
        elif folder_state == 'TRASHED':
            self.delete_associated_task(row.task_id)
            # self.delete_trashed_folder(row.folder_id)
            delete_db_record(row.id, row.folder_id)
            return False
        elif folder_state == 'FOUND':
            return True

    def folder_state(self, folder_id) -> str:
        try:
            response = self.instance.call_webhook(self.webhook_code, 'disk.folder.get',
                                                 params={'id': folder_id})
            if response['result']['DELETED_TYPE'] == '3':
                return 'TRASHED'
            elif response['result']['DELETED_TYPE'] == '0':
                return 'FOUND'
        except KeyError as e:
            if response['error'] == 'ERROR_NOT_FOUND':
                return 'NOT_FOUND'
            exit(e, 'something wrong')

    def delete_associated_task(self, task_id):
        self.instance.call_webhook(self.webhook_code,'task.item.delete',
                                  params={'TASKID': task_id})

    def delete_trashed_folder(self, folder_id):
        self.instance.call_webhook(self.webhook_code, 'disk.folder.deletetree',
                                  params={'id': folder_id})
