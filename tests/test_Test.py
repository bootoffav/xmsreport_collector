import unittest
import os
import openpyxl

from datetime import datetime

from app import Test, Item


class TestTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.t = Test()

    @property
    def file(self):
        if not os.path.isfile(TestTest._file):
            downloaded = self.t.download_file(random.choice(self.p.all_excel_files))
            with open(ParserTest._file, 'wb') as file:
                shutil.copyfileobj(downloaded, file)
        return ParserTest._file

    def test_can_load_file_as_workbook(self):
        wb = openpyxl.load_workbook(self.file)
        self.assertIsInstance(wb, openpyxl.Workbook)

    def test_can_parse_name(self):
        # test = Test(self.file)
        self.assertEqual(2, test.name.count('-'))

    def test_can_parse_checking_date(self):
        # test = Test(self.file)
        self.assertIsInstance(test.checking_date, datetime.datetime)

    def test_can_assign_items_to_test(self):
        # test = Test(self.file)
        items = test.items
        self.assertIsInstance(random.choice(items), Item)

    # def test_can_save_test_to_db(self):
    #     # test = Test(self.file)
    #     self.p.save(test)

    #     self.assertTrue(DB.exists(test))
